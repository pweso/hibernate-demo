package priv.pweso.hibernatedemo.cli;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import priv.pweso.hibernatedemo.domain.Student;
import priv.pweso.hibernatedemo.domain.University;
import priv.pweso.hibernatedemo.repository.UniversityRepository;

@Component
@Slf4j
public class Starter implements CommandLineRunner {

    private final UniversityRepository universityRepository;

    public Starter(UniversityRepository universityRepository) {
        this.universityRepository = universityRepository;
    }

    @Override
    public void run(String... args) {

        University university = new University("UW");
        university.addStudent(new Student("Paweł", "123456"));
        university.addStudent(new Student("Marek", "123456"));

        universityRepository.save(university);

        University umk = universityRepository.findByName("UW");
        System.out.println(umk);
    }
}




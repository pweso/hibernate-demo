package priv.pweso.hibernatedemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import priv.pweso.hibernatedemo.domain.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
}

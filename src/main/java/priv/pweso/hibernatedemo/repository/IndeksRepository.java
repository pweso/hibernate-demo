package priv.pweso.hibernatedemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import priv.pweso.hibernatedemo.domain.Indeks;

public interface IndeksRepository extends JpaRepository<Indeks, Long> {
}

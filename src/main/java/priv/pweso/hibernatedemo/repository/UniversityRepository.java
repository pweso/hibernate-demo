package priv.pweso.hibernatedemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import priv.pweso.hibernatedemo.domain.University;

public interface UniversityRepository extends JpaRepository<University, Long> {
    University findByName(String name);
}
